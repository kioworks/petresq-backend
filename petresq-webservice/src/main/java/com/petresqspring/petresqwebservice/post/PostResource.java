package com.petresqspring.petresqwebservice.post;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@CrossOrigin
@RestController
public class PostResource {

    @Autowired
    private PostHardcodedService postService;

    @GetMapping("/users/{username}/posts")
    public List<Post> getAllPosts(@PathVariable String username) {
        // Thread.sleep(3000);
        return postService.findAll();
    }

    @GetMapping("/users/{username}/posts/{id}")
    public Post getPost(@PathVariable String username, @PathVariable long id) {
        // Thread.sleep(3000);
        return postService.findById(id);
    }


    // DELETE /users/{username}/posts/{id}
    @DeleteMapping("/users/{username}/posts/{id}")
    public ResponseEntity<Void> deletePost(@PathVariable String username, @PathVariable long id) {

        Post post = postService.deleteById(id);

        if (post != null) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.notFound().build();
    }


    @PutMapping("/users/{username}/posts/{id}")
    public ResponseEntity<Post> updatePost(
            @PathVariable String username,
            @PathVariable long id, @RequestBody Post post){

        Post postUpdated = (Post)postService.save(post);

        return new ResponseEntity<Post>(post, HttpStatus.OK);
    }

    //@PostMapping("/users/{username}/posts")
    @RequestMapping(value = "/users/{username}/posts", method = RequestMethod.POST)
    public ResponseEntity<Void> updatePost(
            @PathVariable String username, @RequestBody Post post){

        Post createdPost = postService.save(post);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(createdPost.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

}