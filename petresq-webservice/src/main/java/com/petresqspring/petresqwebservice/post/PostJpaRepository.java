package com.petresqspring.petresqwebservice.post;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostJpaRepository extends JpaRepository<Post, Long>{
    List<Post> findByUsername(String username);
}